import time

from labdo.devices import dummy
from labdo import ExperimentWorker
from labdo import dontpanic

# Create an experiment
# When you create a context like this, devices are cleaned up automatically. 10/10 safer.
with ExperimentWorker() as my_cool_experiment:

    # Attach all connected devices of a specific type
    dummy_devs = dummy.all_devices(my_cool_experiment)
    # Attach a specific device by SN
    dummy_othr = dummy.Device(my_cool_experiment, 'DEVICE_SERIAL_NUMBER')  

    # Do something with the devices
    positions = range(5)

    for p in positions:
        for dev in dummy_devs:
            dev.move_to(p)

    dontpanic(dummy_othr.move_to)(100)
    dontpanic(dummy_othr.move_fail)()

    for p in positions:
        for dev in dummy_devs:
            dev.move_to(p)

    print("We're about the deliberately break the experiment. Don't worry!")
    time.sleep(2)
    dummy_othr.move_fail()
