# -*- coding: utf-8 -*-
import labdo
import labdo.devices.serial

import time

class Device(labdo.devices.serial.Device):
    def __init__(self, experiment, com_port, timeout=1):
        labdo.devices.serial.Device.__init__(self, experiment, com_port, baudrate=9600, timeout=timeout)
        
        #Startup stuff
        self.com.write('\r'.encode())
        self.com.flush()
        lazy_start_ans = self.com.readline().decode()
        if lazy_start_ans != "\rCommand error CMD_NOT_DEFINED\n":
            print("WARNING: Device communication may have failed. Response not as expected.")

    # MANDATORY FUNCTIONS
    def teardown(self):
        self.set_enabled(False)
        labdo.devices.serial.Device.teardown(self)


    def panic(self):
        self.set_enabled(False)
        labdo.devices.serial.Device.panic(self)


    # DEVICE FUNCTIONS
    
    # Query functions
    def answer_strip(self, answer_string):
        return int(answer_string.split('\r')[-2])
        
    def get_bool_query(self, command):
        answers = self.query(command)
        if len(answers) > 1:
            print("Got more responses than expected. Status is ambiguous!")
            print(answers)
            return None
        else:
            status = bool(self.answer_strip(answers[0]))
            return status 


    # INTERLOCK STATUS
    def get_interlock(self):
        return self.get_bool_query('interlock?')

    interlock = property(get_interlock)
    
    
    # ENABLED STATUS
    def get_enabled(self):
        return self.get_bool_query('ens?')

    def set_enabled(self, value):
        # Ensure we pass a True/False
        assert isinstance(value, bool)
        
        # Check current status
        current_status = self.get_enabled()
        # If shutter isn't already where you want it
        if current_status != bool(value):
            # Change the shutters position
            return self.write('ens')
        else:
            print("Shutter already there.")
            return None

    enabled = property(get_enabled, set_enabled)

    
def all_devices(experiment):
    print("Not available for generic serial devices.")
    return None