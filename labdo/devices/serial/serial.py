# -*- coding: utf-8 -*-
import labdo
import serial

import time

class Device(labdo.Device):
    def __init__(self, experiment, com_port, baudrate=9600, timeout=1):
        labdo.Device.__init__(self, experiment, com_port)

        # Initialize device
        self.com = serial.Serial(com_port, baudrate=baudrate, timeout=timeout)

        self.serial_number = com_port
        print("Initialized serial device with serial number {}.".format(self.serial_number))


    # MANDATORY FUNCTIONS
    def teardown(self):
        labdo.Device.teardown(self)
        self.close_com()
        print("Serial object {} closed.".format(self.serial_number))


    def panic(self):
        labdo.Device.panic(self)
        self.close_com()
        print("Serial object {} closed.".format(self.serial_number))


    # DEVICE FUNCTIONS
    
    # Close communicatioins
    def close_com(self):
        self.com.close()  # Call pyserial close function on opened port

    # Weite a command, then return the output as an array of lines
    def query(self, command, encode_input=True, add_return=True, return_str='\r', decode_output=True):
        self.write(command, encode_input=encode_input, add_return=add_return, return_str=return_str)
        response = []
        
        ans = self.readline(decode_output=decode_output)
        while ans != '' and ans != b'':
            response.append(ans)
            ans = self.readline(decode_output=decode_output)
        
        return response

    # Write to input buffer
    def write(self, command, encode_input=True, add_return=True, return_str='\r', debug=False):
        if add_return:
            command += return_str
        if encode_input:
            command = command.encode()

        if debug:
            print(command)

        return self.com.write(command)

    # Read line from buffer
    def readline(self, decode_output=True):
        data_encoded = self.com.readline()

        if decode_output:
            return data_encoded.decode()
        else:
            return data_encoded


    # Read bytes from buffer
    def read(self, size=1, decode=True):
        data_encoded = self.com.read(size=size)

        if decode:
            return data_encoded.decode()
        else:
            return data_encoded


def all_devices(experiment):
    print("Not available for generic serial devices.")
    return None