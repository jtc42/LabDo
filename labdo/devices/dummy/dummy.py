# -*- coding: utf-8 -*-
import labdo
import time


class Device(labdo.Device):
    def __init__(self, experiment, serial_number):
        labdo.Device.__init__(self, experiment, serial_number)

        # Initialize device
        self.serial_number = serial_number
        self.position = 0
        print("Initialized dummy device with serial number {}.".format(self.serial_number))

    # MANDATORY FUNCTIONS
    def teardown(self):
        labdo.Device.teardown(self)

        time.sleep(0.5)
        print("Tearing down dummy object {}.".format(self.serial_number))

    def panic(self):
        labdo.Device.panic(self)

        time.sleep(0.5)
        print("Panic-closing dummy object {}.".format(self.serial_number))

    # DEVICE FUNCTIONS
    def move_to(self, position):
        time.sleep(0.1)
        self.position = position
        print("Moving {} to {}".format(self, self.position))
    
    def move_by(self, position):
        time.sleep(0.1)
        self.position += position
        print("Moving {} to {}".format(self, self.position))

    def move_fail(self):
        print("Forcing {} to fail".format(self))
        raise Exception('SURPRISE! YOUR DEVICE BROKE!')


def all_devices(experiment):
    return [Device(experiment, 'ALLDEVICE001'), Device(experiment, 'ALLDEVICE002')]