# -*- coding: utf-8 -*-
import labdo
import time

# Import APT stuff
print("Loading Thorlabs APT device class, based on github.com/qpit/thorlabs_apt.")
# TODO: Handle exception raised when APT dll not found, give a useful message and nicely exit
# TODO: Better integrate qpit/thorlabs_apt in a less lazy way
try:
    from .thorlabs_apt import core as apt_core
    from . import thorlabs_apt as apt
except OSError as e:
    if str(e) == "[WinError 126] The specified module could not be found":
        raise OSError('Error loading apt shared library. Ensure a valid apt.dll is available in an environment path.') from e
    else:
        raise

class Device(labdo.Device, apt.Motor):
    def __init__(self, experiment, serial_number):
        labdo.Device.__init__(self, experiment, serial_number)

        # Initialize device
        apt.Motor.__init__(self, serial_number)

    # MANDATORY FUNCTIONS
    def teardown(self):
        labdo.Device.teardown(self)
        print("Closing has no effect on APT Device class. Cleanup automatic on exit.")

    def panic(self):
        labdo.Device.panic(self)
        print("Closing has no effect on APT Device class. Cleanup automatic on exit.")

    # DEVICE FUNCTIONS
    def move_to(self, position, blocking=True):
        # Override parent method, to default to blocking
        super().move_to(position, blocking=blocking)
    
    def move_by(self, position, blocking=True):
        # Override parent method, to default to blocking
        super().move_by(position, blocking=blocking)

    def move_home(self, blocking=True):
        # Override parent method, to default to blocking
        super().move_home(blocking=blocking)


def all_devices(experiment):
    print("Finding devices...")
    # Get list of connected device objects
    devices = [Device(experiment, dev_id[1]) for dev_id in apt.list_available_devices()]
    
    if not devices:
        print("No devices found!\nEnsure another instance is not running.")
    else:
        print("Devices found:")
        print(devices)
    return devices


# Release all devices
import atexit
@atexit.register
def cleanup():
    print("Cleaning up APT")
    apt_core._cleanup()