# -*- coding: utf-8 -*-
import labdo
import datetime
import time
import numpy as np

# Try importing python-seabreeze module
try:
    # Force cseabreeze backend
    # import seabreeze.cseabreeze
    import seabreeze.spectrometers as sb
except ModuleNotFoundError as e:
    raise Exception("Missing python-seabreeze backend. Ensure ap--/python-seabreeze (poehlmann) is installed.") from e

# Useful functions
# Convert microseconds to seconds
def us2s(us):
    return us/1000000

# Class of spectrometer objects, inherited from SeaBreeze module
class Device(labdo.Device, sb.Spectrometer):
    def __init__(self, experiment, serial_number, device = None):
	
	    # INITIALISE SEABREEZE BEFORE ATTACHING
	    # SEABREEZE SETS SERIAL_NUMBER PROPERTY, SO MUST BE CALLED BEFORE ATTACHING 
		
        # If an explicit device object given
        if device: 
            # Ignore serial_number and initialise with device
            sb.Spectrometer.__init__(self, device)  
            # If the device used to initialize, and the passed serial number don't match
            if self.serial_number != serial_number:  
                print("Warning: Passed device object and passed serial number do not match.")

        # If initialising with a serial number (string) (typical)
        else:  
            device_found = False # Track if a matching device is found
            # TODO: Handle no device matching this serial_number
            for dev in sb.lib.device_list_devices():
                if dev.serial == str(serial_number):
                    if sb.lib.device_is_open(dev):
                        raise Exception("Could not initialize device: Device '%s' already opened." % serial_number)
                    else:
                        sb.Spectrometer.__init__(self, dev)
                        device_found = True
            if not device_found:
                raise Exception("Could not initialize device: No device attached with serial number '%s'." % serial_number)

        print("Initialized device '%s'." % serial_number)
        labdo.Device.__init__(self, experiment, serial_number)

        print("Resetting integration time to minimum")
        self.set_integration(self.minimum_integration_time_micros)
        self.inuse = False  # Initial flag for spectrometer being in use


    # MANDATORY FUNCTIONS
    def teardown(self):
        labdo.Device.teardown(self)
        # Sleep for 1 second + 1 integration time to let thread flush
        time.sleep(1+us2s(self.integration_time)) 
        # Close device
        sb.Spectrometer.close(self)

    def panic(self):
        labdo.Device.panic(self)
        self.teardown(self)

    # DEVICE FUNCTIONS

    # Set and store integration time
    def set_integration(self, t):
        #Calculate time in microseconds
        self.integration_time_micros(t)
        #Store time in microseconds
        self.integration_time = t

    # Check for saturation
    def is_saturated(self):
        max_counts = 200000
        _, y = self.get_spectrum(averages = 1)
        if max(y) >= 0.9*max_counts:
            return 1
        else:
            return 0

    # Automatically set integration time based on saturation
    def auto_integration(self, start_us, step_us):
        current_us = start_us
        self.set_integration(current_us)
        time.sleep(us2s(current_us))
        
        while self.is_saturated():
            print("Saturated. Reducing integration time to {}us".format(current_us))
            
            current_us -= step_us
            self.set_integration(current_us)
            
        print("Integration time set to {}us".format(current_us))
        
        return current_us
            
            
    # Record spectrum with averaging and safe-wait
    def get_spectrum(self, averages = 1):
        while self.inuse:  # Hold while waiting for another process to finish using device
            # TODO: Add timeout here.
            pass
        
        # Store wavelengths
        x = self.wavelengths()
        ys = []
        
        # Averages
        self.inuse = True
        for i in range(averages):
            # Pause for acquisition
            time.sleep(us2s(2*self.integration_time))

            try:  # Catch errors in SeaBreeze function, so inuse flag can be released
                # Record spectrum to list for averaging
                ys.append(self.intensities())  # SeaBreeze function to obtain data for last run
            except Exception as e:
                self.inuse = False  # Release inuse flag
                raise e  # Raise exception
        # Get average spectrum from list of recorded spectra
        y = np.mean(ys, axis=0)
       
        self.inuse = False
        return [x, y]

    # Save a one-shot spectrum to a text file, with filename given as argument
    def save_spectrum(self, averages = 1, fname=""):
        # TODO: Fix super bad bug where if you give averages as anything other than an int, 
        # the spectrometer shits itself and everything breaks really badly
        # TODO: Clean up/modernise name string formatting
        # TODO: Save to directory

        x, y = self.get_spectrum(averages = averages)
        
        dt = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        comments = self.model + "\n" + self.serial_number + "\n" + "Int(us): " + str(self.integration_time) + " Averages: " + str(averages) + "\n"
        header = "Wavelength (nm), Intensity (counts)"
        
        if fname:
            filename = fname
        else:
            filename = self.model + "_" + dt + "_" + fname + ".csv"
        
        #Save file
        np.savetxt(filename, list(zip(x,y)), delimiter=',', fmt='%f', header=header, comments=comments)

        print("File "+filename+" saved")


# Get list of all connected devices
def all_devices(experiment):
    print("Finding devices...")
    # Get list of connected device objects
    devices = [Device(experiment, None, device = dev_id) for dev_id in sb.lib.device_list_devices()]

    if not devices:
        print("No devices found!\nEnsure another instance is not running.")
    else:
        print("Devices found:")
        print(devices)

    return devices