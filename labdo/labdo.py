from __future__ import print_function

from PyQt5 import QtCore, QtWidgets, uic
from PyQt5.QtCore import pyqtSlot, pyqtProperty, pyqtSignal, pyqtBoundSignal
from uuid import uuid4
import types
import time
import warnings

import sys

# TODO: Handle keyboard interrupts on worker thread 
# TODO: Clean up exception handling in worker thread
# TODO: Check for, and move, Experiment-only code left on the generic Worker class

# TODO: Port over the remaining device classes from old PyATER code

# TODO: Discuss moving exception override to ExperimentWorker, so only overrides when necesarry?
# TODO: Discuss possibility of separate Experiment, Worker, and ExperimentWorker classes?

# TODO: Prevent devices from being imported into an ExperimentWorker if they don't contain specific panic and teardown methods


"""
DEV NOTES

- JTC: 
    Yes, class methods use mixedCase, but PEP-8 says:
    "mixedCase is allowed only in contexts where that's already the prevailing 
    style (e.g. threading.py), to retain backwards compatibility."
    PyQt uses mixedCase everywhere, so shut up.

- JTC: 
    The TODO list is getting huge, but it's mostly easy things to implement now. I'm okay with this.

- JTC:
    I'm adding this utterly useless note so I can test pushing to GitLab after the farce that was my attempt at slick migration.
"""


# Prevent app from closing when worker exception occurs
def labdo_excepthook(type, value, tback):
    # TODO: Option to log to file
    # Call the default handler
    sys.__excepthook__(type, value, tback)
    # TODO: Show a critical error messagebox with OK button preceding quit()?
    # Quit QApplication
    QtWidgets.QApplication.quit()


sys.excepthook = labdo_excepthook


def UiFile(UIfile):
    return uic.loadUiType(UIfile)[0]


def dontpanic(call_function):
    """
    A decorator that wraps the passed in function 
    and surpresses exceptions should one occur
    """

    def wrapper(*args, **kwargs):
        # TODO: Find a way to flag the print statement. 
        # Removed for now because big loops with dontpanics made the comand line messy.
        #print("Calling in ZEN mode")
        try:
            return call_function(*args, **kwargs)
        except Exception as e:
            # log the exception
            print("Something went wrong in {}, but that's okay. Lets move on.".format(call_function.__name__))
            print("The error was \n", e)

            # TODO: Option to log to file

    return wrapper


class App(object):
    def __init__(self, mainWindow, worker_object, *argv):
        self._app = QtWidgets.QApplication(*argv)
        self._thread = QtCore.QThread()
        self._messengerThread = QtCore.QThread()
        self._messenger = Messenger()
        self._messenger.moveToThread(self._messengerThread)
        self._messengerThread.start()

        self.worker = worker_object
        self.worker.setMessenger(self._messenger)
        self.worker.moveToThread(self._thread)

        self._thread.start()
        self._myWindow = mainWindow(self.worker, None)
        self.worker.setup()
        self._messenger.startSignal.connect(self.worker.main)

    def run(self):
        self._messenger.startSignal.emit()
        self._myWindow.show()
        self._app.exec_()


class MessageEvent(object):
    QUEUEALL = 0
    IGNOREIFQUEUED = 1
    MOVETOBACK = 2

    def __init__(self, ID, event_function, widget, signal, eventType, queuePlacement):
        self.ID = ID
        self.function = event_function
        self.widget = widget
        self.signal = signal
        self.eventType = eventType
        self.queuePlacement = queuePlacement


class SignalInfo(QtCore.QObject):
    signalOut = pyqtSignal(MessageEvent)

    def __init__(self, call_function, signal, widget, blockList, parent, eventType, queuePlacement):
        QtCore.QThread.__init__(self, parent)

        self._signal = signal

        assert callable(call_function), "function must be callable"

        try:
            workerID = call_function([], _Check=True)
            if not workerID == parent.worker.ID:
                raise RuntimeError(
                    "Couldn't set up externalConnection worker ID of function doesn't match the assigned worker.")
        except TypeError:
            raise TypeError(
                "Couldn't verify LabDo.GuiCall. the function connected to by externalConnect should be a method of a "
                "worker, with one input of type LabDo.Event and decorated with @LabDo.GuiCall")

        if eventType is None:
            self._eventType = uuid4().hex
        else:
            assert type(eventType) is str, "eventType should be set as a string or None"
            self._eventType = eventType

        if queuePlacement is None:
            self._queuePlacement = MessageEvent.QUEUEALL
        else:
            assert queuePlacement in [MessageEvent.QUEUEALL, MessageEvent.IGNOREIFQUEUED,
                                      MessageEvent.MOVETOBACK], "queuePlacement type not understood."
            self._queuePlacement = queuePlacement

        self._function = call_function
        self._widget = widget
        self._blockList = blockList
        self.signalOut.connect(parent.worker.messenger.addEvent)

    def setFunction(self, function):
        raise RuntimeError('Cannot change the signal function once set')

    def getFunction(self):
        return self._function

    def signaller(self, *args):
        ID = uuid4().hex
        self.parent().block(ID, self._blockList)
        self.signalOut.emit(
            MessageEvent(ID, self._function, self._widget, self._signal, self._eventType, self._queuePlacement))

    function = pyqtProperty(types.FunctionType, getFunction, setFunction)


def GuiCall(func):
    def wrapper(obj, Event, _Check=False):
        if _Check:
            return obj.ID
        func(obj, Event)
        obj.finished.emit(Event.ID)

    return wrapper


class MainWindow(QtWidgets.QMainWindow):
    _signalObjs = []
    publishSignal = pyqtSignal(list)
    _publishWrapFns = []

    def __init__(self, worker, parent=None):

        # Perform standard QMainWindow init
        QtWidgets.QMainWindow.__init__(self, parent)
        self.worker = worker
        self.worker.connectSignals(self)
        self.worker.messenger.connectSignals(self)
        self.publishSignal.connect(self.worker.messenger.publishValue)
        self._blockedList = []
        self.setupUi(self)

    def externalConnect(self, widget, signalName, function, blockList=None, eventType=None, queuePlacement=None):
        signal = getattr(widget, signalName)
        assert (type(signal) is pyqtSignal) or (type(
            signal) is pyqtBoundSignal), "External connect inputs should be (pyqtSignal,function) not (%s,%s)" % (
        type(signal), type(function))
        assert callable(function), "External connect inputs should be (pyqtSignal,function) not (%s,%s)" % (
        type(signal), type(function))
        self._signalObjs.append(SignalInfo(function, signal, widget, blockList, self, eventType, queuePlacement))
        signal.connect(self._signalObjs[-1].signaller)

    def subscribeToStream(self, stream, valueType, subscriber):
        self.worker.messenger.subscribeToStream(stream, valueType, subscriber)

    def publishValue(self, valueName, widget):
        def wrapFn(value):
            self.publishSignal.emit([valueName, value])
            self._publishWrapFns.append(wrapFn)

        widget.valueChanged.connect(wrapFn)
        self.worker.messenger.publishValue([valueName, widget.value()])

    @staticmethod
    def distributeStreams(fnDataPair):
        fnDataPair[0](fnDataPair[1])

    def mousePressEvent(self, event):
        focused_widget = QtWidgets.QApplication.focusWidget()
        if focused_widget is not None:
            focused_widget.clearFocus()

    def block(self, ID, blockList):
        if blockList is None:
            return
        blockedWidgets = [blockedWidget['obj'] for blockedWidget in self._blockedList]
        for widget in blockList:
            if widget in blockedWidgets:
                self._blockedList[blockedWidgets.index(widget)]['IDs'].append(ID)
            else:
                self._blockedList.append({'obj': widget, 'prevState': widget.isEnabled(), 'IDs': [ID]})
                widget.setEnabled(False)

    def unblock(self, ID):
        cleared = []
        for i, block in enumerate(self._blockedList):
            if ID in block['IDs']:
                block['IDs'].remove(ID)
                if not block['IDs']:
                    cleared.append(i)
        for i in sorted(cleared, reverse=True):
            self._blockedList[i]['obj'].setEnabled(self._blockedList[i]['prevState'])
            del (self._blockedList[i])

    # TODO: Make Close Event optional
    def closeEvent(self, event):
        result = QtWidgets.QMessageBox.question(self,
                                                "Confirm Exit...",
                                                "Are you sure you want to exit?\n",
                                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)

        if result == QtWidgets.QMessageBox.Yes:
            # TODO: Do we need to reintroduce the thread _exited flag, so this waits for worker to finish?

            # Flag worker thread to cleanly stop running
            print("Flagging worker for close")
            self.worker._running = False

            # Perform standard QMainWindow close Event
            print("Running Qt closeEvent")
            QtWidgets.QMainWindow.closeEvent(self, event)
        else:
            event.ignore()


class Messenger(QtCore.QObject):
    startSignal = pyqtSignal()
    removed = pyqtSignal(str)
    streamSignal = pyqtSignal(list)
    values = {}

    def __init__(self, *args):
        QtCore.QObject.__init__(self, *args)
        self._eventQueue = []
        self.GUI = None
        self._streams = []

    def connectSignals(self, GUI):
        self.GUI = GUI
        self.removed.connect(GUI.unblock)
        self.streamSignal.connect(GUI.distributeStreams)

    def addEvent(self, Event):
        if Event.queuePlacement == MessageEvent.QUEUEALL:
            self._eventQueue.append(Event)
        elif (Event.queuePlacement == MessageEvent.IGNOREIFQUEUED) or (Event.queuePlacement == MessageEvent.MOVETOBACK):
            IDs = [e.eventType for e in self._eventQueue]
            if Event.eventType in IDs:
                if Event.queuePlacement == MessageEvent.MOVETOBACK:
                    # TODO: make this less ugly than sin
                    emptied = False
                    while not emptied:
                        try:
                            pos = IDs.index(Event.eventType)
                            poppedEvent = self._eventQueue.pop(pos)
                            self.removed.emit(poppedEvent.ID)
                            del (IDs[pos])
                        except ValueError:
                            emptied = True

                    self._eventQueue.append(Event)
            else:
                self._eventQueue.append(Event)
        else:
            raise RuntimeError("queuePlacement unrecognised")

    def subscribeToStream(self, stream, valueType, subscriber):
        assert type(valueType) is type, "Stream valueType must be a type."
        assert callable(subscriber), "Stream subscriber must be callable"
        self._streams.append({'stream': stream, 'type': valueType, 'function': subscriber})

    def publishToStream(self, stream, value):
        # TODO: Once first connection is established stop searching every time
        streamNames = [s['stream'] for s in self._streams]
        ind = -1
        while stream in streamNames[ind + 1:]:
            ind += streamNames[ind + 1:].index(stream) + 1
            stream_dict = self._streams[ind]
            self.streamSignal.emit([stream_dict['function'], stream_dict['type'](value)])

    def publishValue(self, value):
        self.values[value[0]] = value[1]

    def readValue(self, valueName):
        return self.values[valueName]

    def retrieveEvent(self):
        if not len(self._eventQueue) == 0:
            return self._eventQueue.pop()
        else:
            return None


class Worker(QtCore.QObject):
    string_messenger_warning = "No messenger object attached!\nIf you're making a GUI program, something has gone very wrong!"

    # TODO: Annotate all signals so purpose is clearer
    finished = pyqtSignal(str)

    def __init__(self, *args):
        QtCore.QObject.__init__(self, *args)

        self._running = True  # Allow safe shutdown

        self.ID = uuid4().hex
        self.GUI = None
        self.messenger = None

        self.PANIC = False  # Initially assume thread closes neatly without panic

    def __enter__(self):
        self.__is_context_manager = True
        return self

    def __exit__(self, exc_type, exc_val, exc_traceback):
        pass

    def setMessenger(self, messenger):
        self.messenger = messenger

    def connectSignals(self, GUI):
        self.GUI = GUI
        self.finished.connect(GUI.unblock)

    def setup(self):
        pass

    def main(self):
        """
        Main loop when running as a Qt worker thread
        """
        while self._running:
            # TODO: make this less ugly than sin
            if self.eventsWaiting:
                # Handle exceptions that occur during Events
                try:
                    self.handleEvent()
                except Exception as e:
                    # Flag experiment as PANIC closing 
                    # Overrides ExperimentWorker clean __exit__ on main thread
                    self.PANIC = True
                    # Raise exception in worker thread
                    raise e
            else:
                time.sleep(.01)

        # Set flag that thread has finished
        print("Exiting thread")

    def publishToStream(self, stream, value):
        if self.messenger is not None:
            self.messenger.publishToStream(stream, value)
        else:
            warnings.warn(self.string_messenger_warning)

    def readValue(self, valueName):
        if self.messenger is not None:
            return self.messenger.readValue(valueName)
        else:
            warnings.warn(self.string_messenger_warning)
            return None

    def handleEvent(self):
        if self.messenger is not None:
            Event = self.messenger.retrieveEvent()
            Event.function(Event)
        else:
            warnings.warn(self.string_messenger_warning)
            return None

    def __getEventsWaiting(self):
        if self.messenger is not None:
            return len(self.messenger._eventQueue) > 0
        else:
            warnings.warn(self.string_messenger_warning)
            return None

    @staticmethod
    def __setEventsWaiting():
        print("Ignored! You can't set events!")

    eventsWaiting = pyqtProperty(bool, __getEventsWaiting, __setEventsWaiting)


class ExperimentWorker(Worker):
    def __init__(self, *args):
        Worker.__init__(self, *args)
        self.__is_context_manager = False  # Assume not created through context manager
        self.devices = []

    def __enter__(self):
        # Extends parent Worker enter
        Worker.__enter__(self)

    def __exit__(self, exc_type, exc_val, exc_traceback):
        # Overrides parent Worker exit
        if (exc_traceback is None) and (self.PANIC == False):
            print("Exiting experiment neatly.")
            self.teardown()
            print("Teardown finished.")
            return True
        else:
            print("Something went critically wrong. Attempting to close safely.")
            self.panic()
            return False

    def attach(self, device):
        self.devices.append(device)
        print("Device {} attached to experiment {}.".format(device, self))

    # Override default worker teardown method
    def teardown(self):
        print("Tearing down experiment worker thread.")
        for device in self.devices:
            print("Closing device {}...".format(device))
            device.teardown()
            print("Device {} Closed".format(device))

    def panic(self):
        # TODO: For each device, try to close, and handle nicely is closing raises an exception
        for device in self.devices:
            if not device.dontpanic:
                print("Closing device {}...".format(device))
                device.panic()
                print("Device {} Closed".format(device))
            else:  # If device is set to ignore Experiment panics
                print("Ignoring device {}, due to dontpanic flag!".format(device))


class Device:
    def __init__(self, experiment, serial_number):
        # If attaching to a valid experiment object
        if isinstance(experiment, ExperimentWorker):
            experiment.attach(self)
            # Initial flags
            self.dontpanic = False  # Assume that experiment panic should shut down the device
        else:
            raise Exception("Attaching device to a non-Experiment object is forbidden.")

    # MANDATORY FUNCTION
    def teardown(self):
        print("Tearing down {}".format(self))

    def panic(self):
        print("Panic-closing {}".format(self))
