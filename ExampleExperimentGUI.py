#!/usr/bin/env python

from __future__ import print_function
import math
import random
import time
import sys
import os
import traceback
import numpy as np

import labdo as ld
from labdo.devices import dummy
from labdo import dontpanic

from PyQt5 import QtCore
import numpy as np

ui_file = ld.UiFile("ExampleExperimentGUI.ui")


# Create UI class
class MyWindowClass(ld.MainWindow, ui_file):
    timer = QtCore.QTimer()

    def __init__(self, worker, parent=None):
        ld.MainWindow.__init__(self, worker, parent)
        self.timer = QtCore.QTimer()

        # Store a list of widgets associated with moving the stage
        moveWidgets = [self.UpBtn, self.DownBtn, self.LeftBtn, self.RightBtn, self.InBtn, self.OutBtn,
                       self.ExceptionBtn, self.ZenBtn]

        # Bind intensity measurement (on worker thread) to timer (on UI thread)
        self.externalConnect(self.timer, "timeout", self.worker.read_intensity,
                             queuePlacement=ld.MessageEvent.MOVETOBACK)

        # Bind move buttons to functions
        self.externalConnect(self.UpBtn, "pressed", self.worker.move, blockList=moveWidgets)
        self.externalConnect(self.DownBtn, "pressed", self.worker.move, blockList=moveWidgets)
        self.externalConnect(self.LeftBtn, "pressed", self.worker.move, blockList=moveWidgets)
        self.externalConnect(self.RightBtn, "pressed", self.worker.move, blockList=moveWidgets)
        self.externalConnect(self.InBtn, "pressed", self.worker.move, blockList=moveWidgets)
        self.externalConnect(self.OutBtn, "pressed", self.worker.move, blockList=moveWidgets)

        # Bind exception buttons to functions
        self.externalConnect(self.ExceptionBtn, "pressed", self.worker.raise_exception, blockList=moveWidgets)
        self.externalConnect(self.ZenBtn, "pressed", self.worker.raise_dontpanic_exception, blockList=moveWidgets)

        # Bind plot update timer to update function
        self.timer.timeout.connect(self.update_plots)
        self.timer.start(500)

        # Set up stream subscriptions
        self.publishValue('Nsteps', self.nsteps)  # Number of steps to move by
        self.subscribeToStream('Intensity', float, self.Intensity.setValue)  # Update Intensity textbox
        self.subscribeToStream('Intensity', float, self.update_intensities)  # Update intensity list for plot

        # Set up plot
        self.Plot.setMouseEnabled(x=False, y=False)
        self.line1 = None

        self.I = []
        self.plt_length = 40

    # Define function to add latest intensity to the stored array
    def update_intensities(self, value):
        self.I.append(value)
        if len(self.I) >= self.plt_length:
            del self.I[0]

    # Define function to update plot. Called by self.timer
    def update_plots(self):
        downsample = len(self.I) > 100
        if self.line1 is None:
            self.line1 = self.Plot.plot(range(len(self.I)), self.I, pen='r')
        else:
            self.line1.setData(range(len(self.I)), self.I, autoDownsample=downsample)


# Create worker class
class MyWorkerClass(ld.ExperimentWorker):

    def setup(self):
        # Set up some fake devices.
        self.dummy_devs = dummy.all_devices(self)
        self.GUI.led.On = True

    @ld.GuiCall
    def raise_exception(self, Event):
        print("Deliberately breaking a device")
        self.dummy_devs[0].move_fail()

    @ld.GuiCall
    @dontpanic  # Exceptions are handled quietly and without panicking the experiment
    def raise_dontpanic_exception(self, Event):
        print("Breaking the device in ZEN mode")
        self.dummy_devs[0].move_fail()

    @ld.GuiCall
    def move(self, Event):
        steps = self.readValue('Nsteps')
        if Event.widget == self.GUI.UpBtn:
            direction = 'up'
        elif Event.widget == self.GUI.DownBtn:
            direction = 'down'
        elif Event.widget == self.GUI.LeftBtn:
            direction = 'left'
        elif Event.widget == self.GUI.RightBtn:
            direction = 'right'
        elif Event.widget == self.GUI.InBtn:
            direction = 'in'
        elif Event.widget == self.GUI.OutBtn:
            direction = 'out'
        else:
            assert False, "mystery"

        print("Fake moving {} by {} steps".format(direction, steps))
        time.sleep(1)
        print("Finished fake move")

    @ld.GuiCall
    def read_intensity(self, Event):
        intensity_value = np.sin((time.time() / 2.) + 0.3 * random.random())  # Emulate a noisy sine wave
        self.publishToStream('Intensity', intensity_value)


with MyWorkerClass() as experiment_worker:
    app = ld.App(MyWindowClass, experiment_worker, sys.argv)
    app.run()
